function standard_normal_cdf(to)
{
    var mean = 0.0;
    var sigma = 1.0;

    var z = (to - mean) / Math.sqrt(2*sigma*sigma);
    var t = 1 / (1 + 0.3275911 * Math.abs(z));
    var a1 =  0.254829592;
    var a2 = -0.284496736;
    var a3 =  1.421413741;
    var a4 = -1.453152027;
    var a5 =  1.061405429;
    var erf = 1 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1) * t * Math.exp(-z * z);
    var sign = 1;
    if (z < 0)
    {
        sign = -1;
    }
    return (1/2)*(1+sign*erf);
}

/**
 * @param spot Current Spot Price of Underlying
 * @param strike Strike Price of the Call
 * @param tte Time to Expiry [days]
 * @param stddev Standard Deviation of Underlying's returns
 * @param rfr Current Risk-Free Rate
 */
function call_price(spot, strike, tte, stddev, rfr)
{
    // see http://en.wikipedia.org/wiki/Black-Scholes_model as of 2014-05-05

    // tte in days is way more useful than tte in years but the formulate uses years
    // let us do a simple conversion here

    tte = tte / 365.0;

    var d1 = 1.0 / (stddev * Math.sqrt(tte)) * (Math.log(spot / strike) + ((rfr + 0.5 * (stddev * stddev)) * tte));
    var d2 = d1 - stddev * Math.sqrt(tte);

    var call = standard_normal_cdf(d1) * spot - standard_normal_cdf(d2) * strike * Math.exp(-rfr * tte);
    return call;
}

/**
 * @see call_price
 */
function put_price(spot, strike, tte, stddev, rfr)
{
    // Assumes perfect put call parity

    // This assumption may be very bold! In practice the volatility rise when
    // underlying price falls needs to be accounted for (and the vol drop when price rises).
    // See 1987 crash and why volatility skew was introduced.

    var put = strike * Math.exp(-rfr * tte / 365.0) - spot + call_price(spot, strike, tte, stddev, rfr);
    return put;
}

function linear_motion_prices(from_spot, to_spot, days)
{
    var daily_delta = (to_spot - from_spot) / (days - 1);

    var ret = new Array();
    var spot = from_spot;
    for (var i = 0; i < days; i++)
    {
        ret.push(spot);
        spot = spot + daily_delta;
    }

    return ret;
}

function brownian_recursive_split(move, depth, daily_deviation)
{
    if (depth == 1)
    {
        var ret = new Array();
        ret.push(move);
        return ret;
    }

    var right = -Math.random() * daily_deviation * move;
    var left = move - right;

    var left_depth = parseInt(Math.ceil(depth / 2.0));
    var right_depth = depth - left_depth;

    var left_split = brownian_recursive_split(left, left_depth, daily_deviation);
    var right_split = brownian_recursive_split(right, right_depth, daily_deviation);

    return left_split.concat(right_split);
}

function smooth_brownian_deltas(deltas)
{
    var max = 0;
    var max_idx = -1;

    for (var i = 0; i < deltas.length; i++)
    {
        if (Math.abs(deltas[i]) > max)
        {
            max = Math.abs(deltas[i]);
            max_idx = i;
        }
    }

    var spread_factor = deltas[max_idx] / deltas.length;
    for (var i = 0; i < deltas.length; i++)
    {
        if (i == max_idx)
            deltas[i] = spread_factor;
        else
            deltas[i] += spread_factor;
    }

    return deltas;
}

// taken from https://github.com/coolaj86/knuth-shuffle/blob/master/index.js
function shuffle(array)
{
    var currentIndex = array.length
        , temporaryValue
        , randomIndex
        ;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function brownian_motion_prices(from, to, days, daily_deviation)
{
    var brownian_deltas = brownian_recursive_split(to - from, days, daily_deviation);
    for (var i = 0; i < Math.log(10*days); i++)
        smooth_brownian_deltas(brownian_deltas);
    shuffle(brownian_deltas);
    var ret = new Array();

    ret.push(from);
    var spot = from;
    for (var i = 0; i < days; i++)
    {
        spot = spot + brownian_deltas[i];
        ret.push(spot);
    }

    return ret;
}
